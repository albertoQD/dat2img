#include <iostream>
#include <fstream>
#include <bitset>
#include <cstdlib>
#include <string>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

int main(int argc, char **argv) {
    
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " <input_file> <output_file> <image_width> <image_height>" << std::endl;
        exit(1);
    }
    
    std::fstream fin;
    fin.open(argv[1], std::fstream::in);
    
    if (!fin.is_open()) {
        std::cerr << "[ERROR] Could not open the input file!" << std::endl;
        exit(1);
    }
    
    int width = std::atoi(argv[3]);
    int height = std::atoi(argv[4]);
    
    cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_64F);
    std::string  line;
    
    for (int i=0; i<height; i+=1)
    {
        for (int j=0; j<width; j+=1)
        {
            if (!(fin >> line))
            {
                std::cerr << "[ERROR] Reading the file. The given dimensions do not match" << std::endl;
                fin.close();
                return 1;
            }
            std::bitset<64> bs(line);
            image.at<double>(i, j) = bs.to_ulong();
        }
    }
    
    fin.close();
    
    cv::normalize(image, image, 0.0, 255.0, cv::NORM_MINMAX);
    cv::imwrite(std::string(argv[2]), image);
    
    return 0;
}
